# -*- coding: utf-8 -*-

"""
triangulate.py
This is the main script, where triangulation is initialized
"""

import osgeo.ogr as ogr
import triangulation

ogr.RegisterAll()

'''
import sys
sys.path.append("/home/delaunay/Documents/TFG/Constraint_Delaunay_Triangulation/")
execfile(u'/home/delaunay/Documents/TFG/Constraint_Delaunay_Triangulation/triangulate.py'.encode('UTF-8'))

execfile(u'G:/TFG/Constraint_Delaunay_Triangulation/triangulate.py'.encode('UTF-8'))
execfile(u'D:/david.lopez/TFG/Constraint_Delaunay_Triangulation/triangulate.py'.encode('UTF-8'))
'''
# Uncomment depending on OS used
# HOME_PATH = "G:/TFG"
# HOME_PATH = "D:/david.lopez/TFG"
#HOME_PATH = "/home/ronaldo/Documents/TFG"
HOME_PATH = "/home/delaunay/Documents/TFG"
CLOUDPOINTS_LAYER_PATH = HOME_PATH + "/Sample_Data/nube_TFG_prueba3.shp"
#CLOUDPOINTS_LAYER_PATH = HOME_PATH + "/Sample_Data/Las_Huebras_Murcia_puntos_relleno_2.shp"
BREAKLINES_LAYER_PATH = HOME_PATH + "/Sample_Data/breaklines_TFG_prueba3.shp"
#BREAKLINES_LAYER_PATH = HOME_PATH + "/Sample_Data/Las_Huebras_lineas_rotura_2.shp"
#HULLS_LAYER_PATH = HOME_PATH + "/Sample_Data/hulls_TFG_prueba.shp"
HULLS_LAYER_PATH = HOME_PATH + "/Sample_Data/islands_TFG_prueba.shp"
#HULLS_LAYER_PATH = HOME_PATH + "/Sample_Data/Las_Huebras_islas_2.shp"
BOUNDARY_LAYER_PATH = HOME_PATH + "/Sample_Data/contorno_TFG_prueba.shp"
OUTPUT_TRIANGLES_PATH = HOME_PATH + "/Sample_Data/SALIDA/salida_triangulos.shp"
OUTPUT_SEGMENTS_PATH = HOME_PATH + "/Sample_Data/SALIDA/salida_segmentos.shp"
METHOLOGY = "Metodologia Uno"
ADJACENCY_LEVEL = 6

# Read points cloud
datasource_points = ogr.Open(CLOUDPOINTS_LAYER_PATH)
layer_points = datasource_points.GetLayer(0)

# Read breaklines
datasource_breaklines = ogr.Open(BREAKLINES_LAYER_PATH)
layer_breaklines = datasource_breaklines.GetLayer(0)

# Read hulls
datasource_hulls = ogr.Open(HULLS_LAYER_PATH)
layer_hulls = datasource_hulls.GetLayer(0)

# Read boudary
#datasource_boundary = ogr.Open(BOUNDARY_LAYER_PATH)
#layer_boundary = datasource_boundary.GetLayer(0)
layer_boundary = None


'''
# Read breaklines
datasource_segments = ogr.Open(BREAKLINES_LAYER_PATH)
layer_segments = datasource_segments.GetLayer(0)

features_segments = []
for feature in layer_segments:
    features_segments.append(feature.GetGeometryRef())
del (datasource_segments, layer_segments)

# Read hulls
datasource_hulls = ogr.Open(HULLS_LAYER_PATH)
layer_hulls = datasource_hulls.GetLayer(0)

features_hulls = []
for feature in features_hulls:
    features_hulls.append(feature.GetGeometryRef())
del (datasource_hulls, layer_hulls)

# Read boundary
datasource_boundary = ogr.Open(BOUNDARY_LAYER_PATH)
layer_boundary = datasource_boundary.GetLayer(0)

feature_boundary = []
for feature in feature_boundary:
    feature_boundary.append(feature.GetGeometryRef())
del (datasource_boundary, layer_boundary)
'''


"""
Once data are loaded, it initialices the triangulation
"""


triangulation_ = triangulation.Triangulation(layer_points,
                                             layer_breaklines,
                                             layer_hulls,
                                             layer_boundary,
                                             OUTPUT_TRIANGLES_PATH,
                                             OUTPUT_SEGMENTS_PATH,
                                             METHOLOGY,
                                             ADJACENCY_LEVEL)
triangulation_.triangulate()
