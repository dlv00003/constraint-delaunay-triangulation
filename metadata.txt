# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Constraint Delaunay Triangulation
qgisMinimumVersion=3.0
description=It computes a Constraint Delaunay Triangulation. Land Surveryor Use-Case
version=0.9
author=David López Villegas
email=dlv00003@red.ujaen.es

about=Input: - Mass Point Layer. It mustn't be two points in the same position. - Breakline Layer. Breaklines mustn't cross any other geometry. - Hull Layer. Hulls mustn't cross any other geometry. Output: - Generates a Polyline Layer whit the TIN. - Topology is kept in these features

tracker=http://bugs
repository=http://repo
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=topography

homepage=http://homepage
category=Plugins
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

