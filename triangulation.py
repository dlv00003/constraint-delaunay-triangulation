# -*- coding: utf-8 -*-

"""
File: triangulation.py
This class defines the environment where data is stored, it manages other
classes to build the whole triangulation
"""

from . import segment
from . import triangle
import math
import osgeo.ogr as ogr
import os.path
ogr.UseExceptions()


class Triangulation:

    def __init__(self,
                 layer_points_,
                 layer_breaklines_,
                 layer_hulls_,
                 boundary,
                 output_triangles,
                 output_segments,
                 methology,
                 adjacency):
        """
        Given a list of Points, Breaklines, Hulls and a Boundary, creates the
        ficticial triangles involved
        :param layer_points_: ogr.Layer()
        :param layer_breaklines_: ogr.Layer()
        :param layer_hulls_: list()
        :param boundary: ogr.Layer()
        :param output_triangles: str()
        :param output_segments: str()
        :param methology: str()
        :param adjacency: int()
        """

        self._triangles = {}
        self._breaklines = {}
        self._hulls = {}
        self._features_points = []
        self._features_breaklines = []
        self._features_hulls = []
        self._geom_boundary = None
        self._adjacency = adjacency
        self._max_id_triangles = 1
        self._max_id_breakline = 0
        self._max_id_hull = 0
        self._output_triangles = output_triangles
        self._output_segments = output_segments

        if boundary is not None:
            features_points = []
            features_breaklines = []
            features_hulls = []

            for feature in layer_points_:
                features_points.append(feature)
                # self._features_points.append(feature)
            for feature in layer_breaklines_:
                features_breaklines.append(feature)
                # self._features_breaklines.append(feature)
            for feature in layer_hulls_:
                features_hulls.append(feature)
                # self._features_hulls.append(feature)
            for feature in boundary:
                geom = feature.GetGeometryRef()
                self._geom_boundary = ogr.CreateGeometryFromWkt(geom.ExportToWkt())

            '''
            Filter points out of boundary
            '''

            for feature in features_points:
                geom = feature.GetGeometryRef()
                if geom.Within(self._geom_boundary):
                    self._features_points.append(feature)
            if len(self._features_points) == 0:
                raise Exception("Boundary contains no points")

            '''
            Filter breaklines that intersects or are out of boundary
            '''

            for feature in features_breaklines:
                geom = feature.GetGeometryRef()
                if geom.Within(self._geom_boundary):
                    self._features_breaklines.append(feature)
            if len(self._features_breaklines) == 0:
                raise Exception("Boundary contains no breaklines")

            '''
            Filter hulls that intersects or are out of boundary
            '''

            for feature in features_hulls:
                geom = feature.GetGeometryRef()
                if geom.Within(self._geom_boundary):
                    self._features_hulls.append(feature)
            if len(self._features_hulls) == 0:
                raise Exception("Boundary contains no hulls")
        else:
            for feature in layer_points_:
                self._features_points.append(feature)
            for feature in layer_breaklines_:
                self._features_breaklines.append(feature)
            for feature in layer_hulls_:
                self._features_hulls.append(feature)

        '''
        Calculate Ficticial Triangles
        '''

        # It calculates the triangle's vertex which wrap the point layer
        extents = layer_points_.GetExtent()
        xmin = extents[0]
        xmax = extents[1]
        ymin = extents[2]
        ymax = extents[3]

        incx = math.fabs(xmax - xmin) * 2.0
        incy = math.fabs(ymax - ymin) * 2.0

        xmin = xmin - incx
        ymin = ymin - incy
        xmax = xmax + incx
        ymax = ymax + incy

        # Generate the info to build the first triangle
        point_a = ogr.CreateGeometryFromWkt("POINT(%s %s %s)" % (xmin, ymin, 0))
        wkt_point_b = ("POINT(%s " % (xmin + ((xmax - xmin) * 0.5))
                       + "%s " % ymax
                       + "%s)" % 0)
        point_b = ogr.CreateGeometryFromWkt(wkt_point_b)
        point_c = ogr.CreateGeometryFromWkt("POINT(%s %s %s)" % (xmax, ymin, 0))

        segment_a_ = segment.Segment(point_a, point_b, None, None, None)
        segment_b_ = segment.Segment(point_b, point_c, None, None, None)
        segment_c_ = segment.Segment(point_c, point_a, None, None, None)

        self._ficticial_points = [point_a.ExportToWkt(),
                                  point_b.ExportToWkt(),
                                  point_c.ExportToWkt()]
        self._triangles["1"] = triangle.Triangle("1",
                                                 segment_a_,
                                                 segment_b_,
                                                 segment_c_)

    def get_max_id_hull(self):
        """Return the maximum index of key from hulls"""
        return self._max_id_hull

    def get_max_id_breakline(self):
        """Return the maximum index of key from breaklines"""
        return self._max_id_breakline

    def get_max_id_triangle(self):
        """Return the maximum index of key in the triangulation"""
        return self._max_id_triangles

    def set_max_id_hull(self, max_id_hull_):
        """Stablish the maximum index found in hulls"""
        self._max_id_hull = max_id_hull_

    def set_max_id_breakline(self, max_id_breakline):
        """Stablish the maximum index found in breakline"""
        self._max_id_breakline = max_id_breakline

    def set_max_id_triangle(self, max_id_triangle):
        """Stablish the maximum index found in the triangulation"""
        self._max_id_triangles = max_id_triangle

    def get_adjacency(self):
        """Return the adjacency level of the triangulation"""
        return self._adjacency

    def get_triangle_by_id(self, id_):
        """Returns the starting point of the segment"""
        return self._triangles[id_]

    def get_breakline_by_id(self, id_):
        """Returns the breakline segments"""
        return self._breaklines[id_]

    def get_hull_by_id(self, id_):
        """Returns the breakline segments"""
        return self._hulls[id_]

    def append_segment_bk(self,
                          bk_id,
                          triangle_left,
                          segment_id_left,
                          triangle_right,
                          segment_id_right):
        """
        Given two triangles together, build the information regardless to
        segment of a given breakline. The breakline must be already created
        :param bk_id: String. Id of breakline, from self._breaklines.keys(). It
        is the breakline, that is going to be updated, that a new segment is
        appended.
        :param triangle_left: String
        :param segment_id_left: String {a, b or c}
        :param triangle_right: String
        :param segment_id_right: String {a, b or c}
        """

        breakline_ = self.get_breakline_by_id(bk_id)
        segment_ = [triangle_left,
                    segment_id_left,
                    triangle_right,
                    segment_id_right]
        breakline_.append(segment_)
        self._breaklines[bk_id] = breakline_

    def append_segment_hu(self,
                          hu_id,
                          triangle_left,
                          segment_id_left,
                          triangle_right,
                          segment_id_right):
        """
        Given two triangles together, build the information regardless to
        segment of a given hull. The hull must be already created
        :param hu_id: String. Id of hull, from self._hulls.keys(). It
        is the hull, that is going to be updated, that a new segment is
        appended.
        :param triangle_left: String
        :param segment_id_left: String {a, b or c}. It corresponds to one of
        the segments {a, b or c} of triangle_left using
        :param triangle_right: String
        :param segment_id_right: String {a, b or c}
        """

        hull_ = self.get_hull_by_id(hu_id)
        segment_ = [triangle_left,
                    segment_id_left,
                    triangle_right,
                    segment_id_right]
        hull_.append(segment_)
        self._hulls[hu_id] = hull_

    def search_triangle(self, p):
        """
        Search which triangle the point belong to
        :param p: ogr.wkbPoint  Point to query
        :return: [t1(, t2)]  If a point touches a segment, the list will
        record the two triangles keys involved
        """

        triangle_touched = []
        for triangle_ in list(self._triangles.keys()):
            triangle_geometry = self._triangles[triangle_].get_geometry()
            if p.Within(triangle_geometry):
                # Point inside the triangle, Point doesn't touch the triangle
                triangle_touched = [triangle_]
            elif p.Touches(self._triangles[triangle_].get_geometry()):
                # Point inside the triangle, Point touches the triangle
                triangle_touched.append(triangle_)

        if len(triangle_touched) == 1.0:
            triangle_touched.append(None)
            return triangle_touched
        else:
            return triangle_touched

    def update_adjacent_triangle(self, old_triangle, new_triangle, segment_):
        """
        Change the left triangle attribute of a segment of an adjacent
        triangle. It modifies the segment content.
        Given a segment object and a triangle ID of belonging
        triangle(old_triangle), determines the adjacent triangle, it determines
        the segment involved and change left_triangle attribute with new one
        value(new_triangle)
        :param old_triangle: String, triangle_id, it must exists in _triangles
        attribute
        :param new_triangle: String, triangle_id, it must exists in _triangles
        attribute
        :param segment_: segment.Segment segment that is going to set the
        adjacent triangle
        """

        adjacent_triangle_id = segment_.get_left_triangle()
        try:
            adjacent_triangle = self._triangles[adjacent_triangle_id]
            segment_1_adj_tri = adjacent_triangle.get_segment_a()
            if segment_1_adj_tri.get_left_triangle() == old_triangle:
                segment_1_adj_tri.set_left_triangle(new_triangle)
                adjacent_triangle.set_segment_a(segment_1_adj_tri)
                self._triangles[adjacent_triangle_id] = adjacent_triangle
            segment_2_adj_tri = adjacent_triangle.get_segment_b()
            if segment_2_adj_tri.get_left_triangle() == old_triangle:
                segment_2_adj_tri.set_left_triangle(new_triangle)
                adjacent_triangle.set_segment_b(segment_2_adj_tri)
                self._triangles[adjacent_triangle_id] = adjacent_triangle
            segment_3_adj_tri = adjacent_triangle.get_segment_c()
            if segment_3_adj_tri.get_left_triangle() == old_triangle:
                segment_3_adj_tri.set_left_triangle(new_triangle)
                adjacent_triangle.set_segment_c(segment_3_adj_tri)
                self._triangles[adjacent_triangle_id] = adjacent_triangle
        except KeyError:
            '''
            A None id won't update anything
            '''
            pass

    def get_common_segments(self, t1, t2):
        """
        Given a two adyacent triangles, return a list of two segments objects.
        These segments are common
        :param t1: String id
        :param t2: String id
        :return: list of segment.Segment objects
        """

        common_segment_t1 = self._triangles[t1].get_segment_a()
        if common_segment_t1.get_left_triangle() != t2:
            common_segment_t1 = self._triangles[t1].get_segment_b()
        if common_segment_t1.get_left_triangle() != t2:
            common_segment_t1 = self._triangles[t1].get_segment_c()

        common_segment_t2 = self._triangles[t2].get_segment_a()
        if common_segment_t2.get_left_triangle() != t1:
            common_segment_t2 = self._triangles[t2].get_segment_b()
        if common_segment_t2.get_left_triangle() != t1:
            common_segment_t2 = self._triangles[t2].get_segment_c()

        # Check if the triangles aren't together
        if t1 == t2:
            raise RuntimeError("The triangle %s " % t1
                               + "and %s are the same" % t2)

        if (common_segment_t1.get_left_triangle() == t2
                and common_segment_t2.get_left_triangle() == t1):
            pass
        else:
            raise RuntimeError("The triangle %s " % t1
                               + "and %s aren't together" % t2)

        return [common_segment_t1, common_segment_t2]
    
    def get_adjacent_triangles(self, key):
        """
        Given a triangle, return the adyacent triangles
        :param key: String Triangle ID 
        :return: list of Triangle IDs
        """
        tri_object = self.get_triangle_by_id(key)
        seg_a = tri_object.get_segment_a()
        seg_b = tri_object.get_segment_b()
        seg_c = tri_object.get_segment_c()
        adyacent_triangles = list()
        adyacent_triangles.append(seg_a.get_left_triangle())
        adyacent_triangles.append(seg_b.get_left_triangle())
        adyacent_triangles.append(seg_c.get_left_triangle())
        return adyacent_triangles

    def update_z_intersection_points(self, point_list_, segment_):
        """
        Update the Z value of intersection points with interpolated segment high
        :param point_list_: list ogr.Geometry
        :param segment_: ogr.Geometry(ogr.LineString)
        :return: list ogr.Geometry
        """
        start_point = segment_.GetPoint(0)
        start_point_geom = ogr.Geometry(ogr.wkbPoint)
        start_point_geom.AddPoint(start_point[0],
                                  start_point[1],
                                  start_point[2])
        end_point = segment_.GetPoint(1)
        end_point_geom = ogr.Geometry(ogr.wkbPoint)
        end_point_geom.AddPoint(end_point[0],
                                end_point[1],
                                end_point[2])
        updated_points = list()
        for p in point_list_:
            # Determine reduce distance
            dist_start_end_point = start_point_geom.Distance(end_point_geom)
            dist_start_p_point = start_point_geom.Distance(p)

            # Get high
            increment_z = end_point[2] - start_point[2]
            try:
                high = dist_start_p_point * increment_z / dist_start_end_point
            except ZeroDivisionError:
                high = 0.0
            updated_point = ogr.Geometry(ogr.wkbPoint)
            updated_point.AddPoint(p.GetX(), p.GetY(), start_point[2] + high)
            updated_points.append(updated_point)
        return updated_points

    def get_points_intersection(self, segment_, triangle_list_):
        """
        Given a set of triangle intersected by a segment, it determines the
        intersection points.
        :param segment_: ogr.wkbLineString
        :param triangle_list_: list id
        :return: list ogr.Geometry
        """
        intersection_points = list()
        intersection_points_wkt = list()
        start_seg = ogr.Geometry(ogr.wkbPoint)
        end_seg = ogr.Geometry(ogr.wkbPoint)
        start_seg.AddPoint(segment_.GetPoint(0)[0],
                           segment_.GetPoint(0)[1],
                           segment_.GetPoint(0)[2])
        end_seg.AddPoint(segment_.GetPoint(1)[0],
                         segment_.GetPoint(1)[1],
                         segment_.GetPoint(1)[2])
        seg_vertices = [start_seg.ExportToWkt(), end_seg.ExportToWkt()]
        for tri in triangle_list_:
            t = self.get_triangle_by_id(tri)
            t_geom = t.get_geometry()
            intersection = segment_.Intersection(t_geom)
            # Get the vertices of intersection
            start_vertex = ogr.Geometry(ogr.wkbPoint)
            start_vertex.AddPoint(intersection.GetPoint(0)[0],
                                  intersection.GetPoint(0)[1],
                                  intersection.GetPoint(0)[2])
            start_vertex_wkt = start_vertex.ExportToWkt()
            end_vertex = ogr.Geometry(ogr.wkbPoint)
            end_vertex.AddPoint(intersection.GetPoint(1)[0],
                                intersection.GetPoint(1)[1],
                                intersection.GetPoint(1)[2])
            end_vertex_wkt = end_vertex.ExportToWkt()
            if (start_vertex_wkt not in intersection_points_wkt
                    and start_vertex_wkt not in seg_vertices):
                intersection_points.append(start_vertex)
                intersection_points_wkt.append(start_vertex_wkt)
            if (end_vertex_wkt not in intersection_points_wkt
                    and end_vertex_wkt not in seg_vertices):
                intersection_points.append(end_vertex)
                intersection_points_wkt.append(end_vertex_wkt)
        return intersection_points

    def get_intersected_triangles(self,
                                  segment_,
                                  start_triangle_list,
                                  segment_end_point=None):
        """
        Given a segment, it calculates all the triangles that intersect the
        segment. You can provide the initial list of triangle where it is
        calculated which one intersect the segment. This algorithm checks
        adyacent triangles until no more intersections are found.
        :param segment_: ogr.wkbLineString Itersection segment
        :param start_triangle_list: list id. At least one triangle must
        intersect to segment_
        :param segment_end_point: ogr.wkbPoint of ending point of the segment
        :return intersected_triangle: list id
        """

        # Set start_point and end_point
        if segment_.GetPoint(0) == segment_end_point.GetPoint():
            start_point = ogr.Geometry(ogr.wkbPoint)
            start_point.AddPoint(segment_.GetPoint(1)[0],
                                 segment_.GetPoint(1)[1],
                                 segment_.GetPoint(1)[2])
            end_point = ogr.Geometry(ogr.wkbPoint)
            end_point.AddPoint(segment_.GetPoint(0)[0],
                               segment_.GetPoint(0)[1],
                               segment_.GetPoint(0)[2])
        elif segment_.GetPoint(1) == segment_end_point.GetPoint():
            start_point = ogr.Geometry(ogr.wkbPoint)
            start_point.AddPoint(segment_.GetPoint(0)[0],
                                 segment_.GetPoint(0)[1],
                                 segment_.GetPoint(0)[2])
            end_point = ogr.Geometry(ogr.wkbPoint)
            end_point.AddPoint(segment_.Getpoint(1)[0],
                               segment_.GetPoint(1)[1],
                               segment_.GetPoint(1)[2])
        elif segment_end_point is None:
            start_point = ogr.Geometry(ogr.wkbPoint)
            start_point.AddPoint(segment_.GetPoint(0)[0],
                                 segment_.GetPoint(0)[1],
                                 segment_.GetPoint(0)[2])
            end_point = ogr.Geometry(ogr.wkbPoint)
            end_point.AddPoint(segment_.Getpoint(1)[0],
                               segment_.GetPoint(1)[1],
                               segment_.GetPoint(1)[2])
        else:
            raise RuntimeError("segment_end_point dosen't belong to segment_")

        start_point_wkt = start_point.ExportToWkt()
        end_point_wkt = end_point.ExportToWkt()

        # Get the initial triangle
        current_tri = None
        next_triangle = None
        intersects_ = False
        for tri in start_triangle_list:
            tri_object = self.get_triangle_by_id(tri)
            segments = tri_object.get_segments()
            for i in range(0, 3):
                if (segments[i].get_linestring_geometry().Crosses(segment_)
                        and segments[i].get_start_point().ExportToWkt()
                        != start_point_wkt
                        and segments[i].get_end_point().ExportToWkt()
                        != end_point_wkt):
                    current_tri = tri
                    next_triangle = segments[i].get_left_triangle()
                    intersects_ = True
        if intersects_ is False:
            print("Warning, no intersection found, it could be an error")
            return []
            # raise RuntimeError("No triangles intersect segment_")
        intersected_triangles = [current_tri, next_triangle]

        # Loop until end_point_wkt is found
        end_point_found = False
        while end_point_found is False:
            next_triangle_object = self.get_triangle_by_id(next_triangle)
            next_triangle_vertices_wkt = next_triangle_object.get_vertices_wkt()
            for vertex in next_triangle_vertices_wkt:
                if vertex == end_point_wkt:
                    end_point_found = True
                    break
            if end_point_found is True:
                break

            next_triangle_object = self.get_triangle_by_id(next_triangle)
            segments = next_triangle_object.get_segments()
            for i in range(0, 3):
                '''
                Look for triangles that intersect segment_
                '''
                if (segments[i].get_linestring_geometry().Crosses(segment_)
                        and segments[i].get_start_point().ExportToWkt()
                        != start_point_wkt
                        and segments[i].get_end_point().ExportToWkt()
                        != end_point_wkt):
                    if segments[i].get_left_triangle() != current_tri:
                        current_tri = next_triangle
                        next_triangle = segments[i].get_left_triangle()
                        intersected_triangles.append(next_triangle)
                        break
        return intersected_triangles

    def create_check_list(self, created_triangles, adj_):
        """
        Depending on adjacency_level(adj_), get all adjacent triangles to the
        ones created by either insert_point_within or insert_point_touches.
        adj_ = 0 takes only the new created triangles
        :param created_triangles: List of Strings, triangle ID
        :param adj_: Integer, Adjacency Level
        :return triangle_list: list of triangles IDs along with a counter set
        to 0
        """

        triangle_set = set(created_triangles)
        adjacent_triangles = set()
        for i in range(0, adj_):
            for triangle_id in triangle_set:
                # Get segments from triangles
                try:
                    triangle_ = self.get_triangle_by_id(triangle_id)
                    seg1 = triangle_.get_segment_a()
                    seg2 = triangle_.get_segment_b()
                    seg3 = triangle_.get_segment_c()
                    adjacent_triangles.add(seg1.get_left_triangle())
                    adjacent_triangles.add(seg2.get_left_triangle())
                    adjacent_triangles.add(seg3.get_left_triangle())
                except KeyError:
                    '''
                    Sometimes, None Id triangles are requested to find adjacent
                    triangles. None value doesn't exists on triangle dictionary
                    It won't execute anything in this case
                    '''
                    pass
            triangle_set = triangle_set.union(adjacent_triangles)
            adjacent_triangles = set()

        # Remove None value if inserted triangles are located next to borders
        triangle_list = list(triangle_set)
        try:
            triangle_list.remove(None)
        except ValueError:
            # There is no None values in the list
            pass

        '''
        Build the validation list appending  to every triangles counters
        set to 0
        '''
        validation_triangle_list = list()
        for triangle_ in triangle_list:
            validation_triangle_list.append([triangle_, 0])

        return validation_triangle_list

    def flip(self, t1, t2):
        """
        Given a two adjacent triangles, flips the triangles structure
        :param t1: String id
        :param t2: String id
        :return: True if flip hasn't been aborted
        """

        # Build common segments
        common_segments = self.get_common_segments(t1, t2)
        common_seg_t1 = common_segments[0]
        common_seg_t2 = common_segments[1]

        # Build Triangle 1.
        segment_b_t1 = self._triangles[t1].next_segment(common_seg_t1)
        segment_c_t1 = self._triangles[t1].next_segment(segment_b_t1)

        # Build Triangle 2.
        segment_b_t2 = self._triangles[t2].next_segment(common_seg_t2)
        segment_c_t2 = self._triangles[t2].next_segment(segment_b_t2)

        # Check if flip can be done properly
        oposite_p = segment_b_t2.get_end_point()
        t2_right_to_segment_c_t1 = segment_c_t1.is_point_left_side(oposite_p)
        t2_left_to_segment_b_t1 = segment_b_t1.is_point_left_side(oposite_p)
        if t2_left_to_segment_b_t1 == 1 and t2_right_to_segment_c_t1 == 1:
            # Build Triangle a, flipped version of t1
            segment_a_ta = segment.Segment(segment_b_t2.get_end_point(),
                                           segment_c_t1.get_start_point(),
                                           common_seg_t1.get_left_triangle(),
                                           None,
                                           None)
            segment_b_ta = segment_c_t1
            segment_c_ta = segment_b_t2
            ta = triangle.Triangle(t1,
                                   segment_a_ta,
                                   segment_b_ta,
                                   segment_c_ta)

            # Build Triangle b, flipped version of t2
            segment_a_tb = segment.Segment(segment_b_t1.get_end_point(),
                                           segment_c_t2.get_start_point(),
                                           common_seg_t2.get_left_triangle(),
                                           None,
                                           None)
            segment_b_tb = segment_c_t2
            segment_c_tb = segment_b_t1
            tb = triangle.Triangle(t2,
                                   segment_a_tb,
                                   segment_b_tb,
                                   segment_c_tb)

            # Update dictionary
            ta.is_valid()
            tb.is_valid()
            self._triangles[t1] = ta
            self._triangles[t2] = tb

            self.update_adjacent_triangle(t1, t2, segment_b_t1)
            self.update_adjacent_triangle(t2, t1, segment_b_t2)
            return True
        else:
            return False

    def flip_concatenated(self,
                          intersection_seg,
                          triangle_list,
                          nearest_point_,
                          triangle_base):
        """
        In the situacion where a triangle doesn't fulfill Empty Circle Criteria,
        the nearest point from those points inside the circle is evaluated. This
        point belongs to a triangle that isn't adjacent to triangle base, it
        flips the triangles involved consecutively. If there is a segment of
        breakline or hull, the flip isn't executed. It starts to flip from
        ending triangle.
        :param intersection_seg: ogr.wkbLineString
        :param triangle_list: List of id of triangles that can be flipped
        :param nearest_point_: ogr.wkbPoint Nearest point to triangle_base
        centroid
        :param triangle_base: String Triangle ID
        """

        # Get the segments from intersected_triangles
        segment_list = list()
        for triangle_ in triangle_list:
            tri_object = self.get_triangle_by_id(triangle_)
            segment_list.append(tri_object.get_segment_a())
            segment_list.append(tri_object.get_segment_b())
            segment_list.append(tri_object.get_segment_c())

        '''
        Get the segments that crosses intersection_seg and it doesn't belong to 
        hull or breakline
        '''
        nearest_point_wkt = nearest_point_.ExportToWkt()
        segment_tuples = list()
        for seg in segment_list:
            seg_geom = seg.get_linestring_geometry()
            start_p_wkt = seg.get_start_point().ExportToWkt()
            end_p_wkt = seg.get_end_point().ExportToWkt()
            is_breakline = seg.get_breakline()
            is_hull = seg.get_hull()
            if (intersection_seg.Crosses(seg_geom)
                    and not intersection_seg.Touches(seg_geom)
                    and is_breakline is None
                    and is_hull is None
                    and start_p_wkt != nearest_point_wkt
                    and end_p_wkt != nearest_point_wkt):
                intersection_point = intersection_seg.Intersection(seg_geom)
                dist_ = nearest_point_.Distance(intersection_point)
                tuple_ = (seg, dist_)
                segment_tuples.append(tuple_)

        '''
        Flip the triangles that common segments cross intersection_seg, and stop
        when triangle_base is flipped
        '''
        achieve_triangle_base = False
        while achieve_triangle_base is False:
            '''
            Get the triangles that are gonna be flipped, pop from
            intersection_seglist, and flip them
            '''
            try:
                t1_tuple = min(segment_tuples, key=lambda item: item[1])
                segment_tuples.pop(segment_tuples.index(t1_tuple))
                t2_tuple = min(segment_tuples, key=lambda item: item[1])
                segment_tuples.pop(segment_tuples.index(t2_tuple))
            except ValueError:
                raise ValueError("Tupla vacia")
            if not t1_tuple[0].is_segment_aside(t2_tuple[0]):
                raise RuntimeError("Segments aren't together")
            t1_ = t1_tuple[0].get_left_triangle()
            t2_ = t2_tuple[0].get_left_triangle()
            self.flip(t1_, t2_)

            # Check for ending the loop
            if t1_ == triangle_base or t2_ == triangle_base:
                achieve_triangle_base = True

    def check_list(self, check_list_, adj_):
        """
        Check if there is any vertex in the circle and modify the first segment
        that not fulfill the Empty Circle Criteria. If this happend, reinsert
        the evaluated triangle into the check_list_ again. If it is okay, the
        triangle leaves the check_list_. The loop stops when check_list_ is
        empty or convergence is False.
        :param check_list_: list which each entry contains a list with two
        entries; triangle_key and number of validation attempts that has been
        done, initialy set to 0.
        :param adj_: integer Adjacency level that it is going to be considered
        """

        convergence = True
        repetition_limit = 2 * adj_
        # Validation loop
        while len(check_list_) > 0 and convergence is True:
            base_tri = check_list_.pop()
            base_triangle_object = self.get_triangle_by_id(base_tri[0])
            base_tri_vertices = base_triangle_object.get_vertices()
            base_tri_geom = base_triangle_object.get_geometry()
            base_tri_centroid = base_tri_geom.Centroid()
            circle_ = base_triangle_object.get_circunscrit_circle()

            # search for points that are within the circle
            triangle_involved = list()
            points_involved = list()
            for triangle_ in check_list_:
                triangle_object = self.get_triangle_by_id(triangle_[0])
                vertices_ = triangle_object.get_vertices()
                a_state = vertices_[0].Within(circle_)
                b_state = vertices_[1].Within(circle_)
                c_state = vertices_[2].Within(circle_)
                if a_state:
                    wkt_geom = vertices_[0].ExportToWkt()
                    if wkt_geom not in points_involved:
                        points_involved.append(wkt_geom)

                if b_state:
                    wkt_geom = vertices_[1].ExportToWkt()
                    if wkt_geom not in points_involved:
                        points_involved.append(wkt_geom)

                if c_state:
                    wkt_geom = vertices_[2].ExportToWkt()
                    if wkt_geom not in points_involved:
                        points_involved.append(wkt_geom)
                triangle_involved.append(triangle_[0])

            # Picks the nearest point to triangle_base
            distance_tuple_list = list()
            for point_ in points_involved:
                geom_ = ogr.CreateGeometryFromWkt(point_)
                dist_point_triangle = geom_.Distance(base_tri_vertices[0])
                distance_tuple_list.append((geom_,
                                            base_tri_vertices[0],
                                            dist_point_triangle))
                dist_point_triangle = geom_.Distance(base_tri_vertices[1])
                distance_tuple_list.append((geom_,
                                            base_tri_vertices[1],
                                            dist_point_triangle))
                dist_point_triangle = geom_.Distance(base_tri_vertices[2])
                distance_tuple_list.append((geom_,
                                            base_tri_vertices[2],
                                            dist_point_triangle))
            try:
                nearest_tuple = min(distance_tuple_list,
                                    key=lambda item: item[2])
            except ValueError:
                # It is okay if 'min() arg is an empty sequence'
                nearest_tuple = None

            '''
            If nearest_tuple is None, it means that the number of triangles
            inside the circle is 0, the triangle is automatically validated not
            inseting back to the check_list.
            It calculates the nearest vertex, build the intersection segment
            to determine what triangles are involved
            '''
            if nearest_tuple is not None:
                segment_ = ogr.Geometry(ogr.wkbLineString)
                segment_.AddPoint(nearest_tuple[0].GetX(),
                                  nearest_tuple[0].GetY(),
                                  nearest_tuple[0].GetZ())
                segment_.AddPoint(base_tri_centroid.GetX(),
                                  base_tri_centroid.GetY(),
                                  base_tri_centroid.GetZ())

                triangle_involved.append(base_tri[0])
                # Determine triangles that intersect segment
                intersected_ = self.get_intersected_triangles(segment_,
                                                              [base_tri[0]],
                                                              nearest_tuple[0])

                self.flip_concatenated(segment_,
                                       intersected_,
                                       nearest_tuple[0],
                                       base_tri[0])
                # Reinsert base_tri to check_list_
                base_tri[1] += 1
                check_list_.insert(0, base_tri)
                if base_tri[1] == repetition_limit:
                    convergence = False

    def insert_point_within(self, p, t, max_id):
        """
        Add a point to the current triangulation, it involves to create three
        new triangles and deleting the original one
        :param t: String ID of a triangle
        :param p: ogr.wkbPoint
        :param max_id: String triangle highest value from triangles's
        :return: [] Returns the ids of inserted triangles. In a future iteration
        can get the maximun id. Otherwise, if it is determined through max
        function from keys sometimes the maximum id is already removed and
        new triangles are named as a triangle already removed, thus new
        has wrong triangles names.
        """

        ''' 
        Point Within Triangle:
        Generates three new triangles, update adjacent triangles and it deletes
        the original one
        '''
        triangle_a_id = str(int(max_id) + 1)
        triangle_b_id = str(int(max_id) + 2)
        triangle_c_id = str(int(max_id) + 3)

        # Build triangle_a
        segment_a_triangle_a = self._triangles[t].get_segment_a()
        start_point_ = self._triangles[t].get_segment_a().get_end_point()
        segment_b_triangle_a = segment.Segment(start_point_,
                                               p,
                                               triangle_b_id,
                                               None,
                                               None)
        end_point_ = self._triangles[t].get_segment_a().get_start_point()
        segment_c_triangle_a = segment.Segment(p,
                                               end_point_,
                                               triangle_c_id,
                                               None,
                                               None)
        self._triangles[triangle_a_id] = triangle.Triangle(triangle_a_id,
                                                           segment_a_triangle_a,
                                                           segment_b_triangle_a,
                                                           segment_c_triangle_a)
        self._triangles[triangle_a_id].is_valid()

        # Update adjacent triangle_a
        self.update_adjacent_triangle(t, triangle_a_id, segment_a_triangle_a)

        # Build triangle_b
        segment_a_triangle_b = self._triangles[t].get_segment_b()
        start_point_ = self._triangles[t].get_segment_b().get_end_point()
        segment_b_triangle_b = segment.Segment(start_point_,
                                               p,
                                               triangle_c_id,
                                               None,
                                               None)
        end_point_ = self._triangles[t].get_segment_b().get_start_point()
        segment_c_triangle_b = segment.Segment(p,
                                               end_point_,
                                               triangle_a_id,
                                               None,
                                               None)
        self._triangles[triangle_b_id] = triangle.Triangle(triangle_b_id,
                                                           segment_a_triangle_b,
                                                           segment_b_triangle_b,
                                                           segment_c_triangle_b)
        self._triangles[triangle_b_id].is_valid()

        # Update adjacent triangle_b
        self.update_adjacent_triangle(t, triangle_b_id, segment_a_triangle_b)

        # Build triangle_c
        segment_a_triangle_c = self._triangles[t].get_segment_c()
        start_point_ = self._triangles[t].get_segment_c().get_end_point()
        segment_b_triangle_c = segment.Segment(start_point_,
                                               p,
                                               triangle_a_id,
                                               None,
                                               None)
        end_point_ = self._triangles[t].get_segment_c().get_start_point()
        segment_c_triangle_c = segment.Segment(p,
                                               end_point_,
                                               triangle_b_id,
                                               None,
                                               None)
        self._triangles[triangle_c_id] = triangle.Triangle(triangle_c_id,
                                                           segment_a_triangle_c,
                                                           segment_b_triangle_c,
                                                           segment_c_triangle_c)
        self._triangles[triangle_c_id].is_valid()

        # Update adjacent triangle_c
        self.update_adjacent_triangle(t, triangle_c_id, segment_a_triangle_c)

        '''
        Check if there hulls or breaklines involved, and update self._hulls and
        self._breaklines if necessary
        '''

        if segment_a_triangle_a.get_hull() is not None:
            self.update_hull(segment_a_triangle_a.get_hull(),
                             segment_a_triangle_a.get_hull_segment(),
                             t,
                             triangle_a_id)
        if segment_a_triangle_b.get_hull() is not None:
            self.update_hull(segment_a_triangle_b.get_hull(),
                             segment_a_triangle_b.get_hull_segment(),
                             t,
                             triangle_b_id)
        if segment_a_triangle_c.get_hull() is not None:
            self.update_hull(segment_a_triangle_c.get_hull(),
                             segment_a_triangle_c.get_hull_segment(),
                             t,
                             triangle_c_id)
        if segment_a_triangle_a.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_a.get_breakline(),
                                  segment_a_triangle_a.get_breakline_segment(),
                                  t,
                                  triangle_a_id)
        if segment_a_triangle_b.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_b.get_breakline(),
                                  segment_a_triangle_b.get_breakline_segment(),
                                  t,
                                  triangle_b_id)
        if segment_a_triangle_c.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_c.get_breakline(),
                                  segment_a_triangle_c.get_breakline_segment(),
                                  t,
                                  triangle_c_id)

        # Remove original triangle
        del self._triangles[t]

        return [triangle_a_id, triangle_b_id, triangle_c_id]

    def insert_point_touches(self, p, t1, t2, max_id):
        """
        Add a point to the current triangulation, it involves the creation of
        four new triangles because the point is situated on the edge of two
        triangles
        :param p: ogr.wkbPoint
        :param t1: String triangle ID
        :param t2: String triangle ID
        :param max_id: String triangle highest value from triangles's
        dictionary
        """

        '''
        In this situacion, the point that are going to be inserted falls into
        a segment, this means that two triangle are involved. These two
        triangles are removed and four new triangles are inserted instead.
        The information generated from inserting four triangle follows a
        criteria:
        The selected two triangles must reorder two triangles
        '''

        # Build common segments
        common_segments = self.get_common_segments(t1, t2)
        common_segment_t1 = common_segments[0]
        common_segment_t2 = common_segments[1]

        # Build Triangle 1.
        segment_b_t1 = self._triangles[t1].next_segment(common_segment_t1)
        segment_c_t1 = self._triangles[t1].next_segment(segment_b_t1)

        # Build Triangle 2.
        segment_b_t2 = self._triangles[t2].next_segment(common_segment_t2)
        segment_c_t2 = self._triangles[t2].next_segment(segment_b_t2)

        # Generate the new IDs3
        triangle_a_id = str(int(max_id) + 1)
        triangle_b_id = str(int(max_id) + 2)
        triangle_c_id = str(int(max_id) + 3)
        triangle_d_id = str(int(max_id) + 4)

        # Build triangle_a_id
        segment_a_triangle_a = segment_b_t1
        segment_b_triangle_a = segment.Segment(segment_b_t1.get_end_point(),
                                               p,
                                               triangle_b_id,
                                               None,
                                               None)
        segment_c_triangle_a = segment.Segment(p,
                                               segment_b_t1.get_start_point(),
                                               triangle_d_id,
                                               None,
                                               None)

        triangle_a_content = triangle.Triangle(triangle_a_id,
                                               segment_a_triangle_a,
                                               segment_b_triangle_a,
                                               segment_c_triangle_a)
        triangle_a_content.is_valid()
        self._triangles[triangle_a_id] = triangle_a_content

        # Update adjacent triangle a
        self.update_adjacent_triangle(t1, triangle_a_id, segment_a_triangle_a)

        # Build triangle_b_id
        segment_a_triangle_b = segment_c_t1
        segment_b_triangle_b = segment.Segment(segment_c_t1.get_end_point(),
                                               p,
                                               triangle_c_id,
                                               None,
                                               None)
        segment_c_triangle_b = segment.Segment(p,
                                               segment_c_t1.get_start_point(),
                                               triangle_a_id,
                                               None,
                                               None)

        triangle_b_content = triangle.Triangle(triangle_b_id,
                                               segment_a_triangle_b,
                                               segment_b_triangle_b,
                                               segment_c_triangle_b)
        triangle_b_content.is_valid()
        self._triangles[triangle_b_id] = triangle_b_content

        # Update adjacent triangle b
        self.update_adjacent_triangle(t1, triangle_b_id, segment_a_triangle_b)

        # Build triangle_c_id
        segment_a_triangle_c = segment_b_t2
        segment_b_triangle_c = segment.Segment(segment_b_t2.get_end_point(),
                                               p,
                                               triangle_d_id,
                                               None,
                                               None)
        segment_c_triangle_c = segment.Segment(p,
                                               segment_b_t2.get_start_point(),
                                               triangle_b_id,
                                               None,
                                               None)

        triangle_c_content = triangle.Triangle(triangle_c_id,
                                               segment_a_triangle_c,
                                               segment_b_triangle_c,
                                               segment_c_triangle_c)
        triangle_c_content.is_valid()
        self._triangles[triangle_c_id] = triangle_c_content

        # Update adjacent triangle c
        self.update_adjacent_triangle(t2, triangle_c_id, segment_a_triangle_c)

        # Build triangle_d_id
        segment_a_triangle_d = segment_c_t2
        segment_b_triangle_d = segment.Segment(segment_c_t2.get_end_point(),
                                               p,
                                               triangle_a_id,
                                               None,
                                               None)
        segment_c_triangle_d = segment.Segment(p,
                                               segment_c_t2.get_start_point(),
                                               triangle_c_id,
                                               None,
                                               None)

        triangle_d_content = triangle.Triangle(triangle_d_id,
                                               segment_a_triangle_d,
                                               segment_b_triangle_d,
                                               segment_c_triangle_d)
        triangle_d_content.is_valid()

        self._triangles[triangle_d_id] = triangle_d_content

        # Update adjacent triangle d
        self.update_adjacent_triangle(t2, triangle_d_id, segment_a_triangle_d)

        '''
        Check if there hulls or breaklines involved, and update self._hulls and
        self._breaklines if necessary
        '''
        inserted_tri = [triangle_a_id, triangle_b_id, triangle_c_id, triangle_d_id]
        if segment_a_triangle_a.get_hull() is not None:
            self.update_hull(segment_a_triangle_a.get_hull(),
                             segment_a_triangle_a.get_hull_segment(),
                             t1,
                             triangle_a_id)
        if segment_a_triangle_b.get_hull() is not None:
            self.update_hull(segment_a_triangle_b.get_hull(),
                             segment_a_triangle_b.get_hull_segment(),
                             t1,
                             triangle_b_id)
        if segment_a_triangle_c.get_hull() is not None:
            self.update_hull(segment_a_triangle_c.get_hull(),
                             segment_a_triangle_c.get_hull_segment(),
                             t2,
                             triangle_c_id)
        if segment_a_triangle_d.get_hull() is not None:
            self.update_hull(segment_a_triangle_d.get_hull(),
                             segment_a_triangle_d.get_hull_segment(),
                             t2,
                             triangle_d_id)
        if segment_a_triangle_a.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_a.get_breakline(),
                                  segment_a_triangle_a.get_breakline_segment(),
                                  t1,
                                  triangle_a_id)
        if segment_a_triangle_b.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_b.get_breakline(),
                                  segment_a_triangle_b.get_breakline_segment(),
                                  t1,
                                  triangle_b_id)
        if segment_a_triangle_c.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_c.get_breakline(),
                                  segment_a_triangle_c.get_breakline_segment(),
                                  t2,
                                  triangle_c_id)
        if segment_a_triangle_d.get_breakline() is not None:
            self.update_breakline(segment_a_triangle_d.get_breakline(),
                                  segment_a_triangle_d.get_breakline_segment(),
                                  t2,
                                  triangle_d_id)

        # Remove original triangles
        del self._triangles[t1]
        del self._triangles[t2]

        return inserted_tri

    def update_segment_bk(self,
                          triangle_id_1,
                          triangle_id_2,
                          breakline_id):
        """
        Given two triangles and vertices of a segment, find the segment to
        update. Also works with Hulls. Update triangle object directly. Be sure
        to assign correct values to breakline_id. Be sure that triangle_id_1 and
        triangle_id_2 are together. Appends a new entry into breakline
        attribute. To call the segment, it is used the index in the breakline
        list.
        :param triangle_id_1: String ID triangle
        :param triangle_id_2: String ID triangle
        :param breakline_id: String ID of breakline
        """

        t1 = self.get_triangle_by_id(triangle_id_1)
        t2 = self.get_triangle_by_id(triangle_id_2)
        t1_vertices = t1.get_vertices_wkt()
        t2_vertices = t2.get_vertices_wkt()
        segments = t1.get_segments()
        segment_index = len(self.get_breakline_by_id(breakline_id))
        segment_id_1 = None
        segment_id_2 = None

        # Find and update common segment in t1 triangle
        for i in range(0, 3):
            seg = segments[i]
            start_p_seg = seg.get_start_point()
            start_p_seg_wkt = start_p_seg.ExportToWkt()
            end_p_seg = seg.get_end_point()
            end_p_seg_wkt = end_p_seg.ExportToWkt()
            if (start_p_seg_wkt in t2_vertices
                    and end_p_seg_wkt in t2_vertices):
                # Set triangle's segment information
                seg.set_breakline(str(breakline_id), str(segment_index))
                segments[i] = seg
                segment_id_1 = i
                break
        t1.set_segments_by_list(segments)
        self._triangles[triangle_id_1] = t1

        # Find and update common segment in t2 triangle
        segments = t2.get_segments()
        for i in range(0, 3):
            seg = segments[i]
            start_p_seg = seg.get_start_point()
            start_p_seg_wkt = start_p_seg.ExportToWkt()
            end_p_seg = seg.get_end_point()
            end_p_seg_wkt = end_p_seg.ExportToWkt()
            if (start_p_seg_wkt in t1_vertices
                    and end_p_seg_wkt in t1_vertices):
                # Set triangle's segment information
                seg.set_breakline(str(breakline_id), str(segment_index))
                segments[i] = seg
                segment_id_2 = i
                break
        t2.set_segments_by_list(segments)
        self._triangles[triangle_id_2] = t2

        # Set breakline attribute information
        self.append_segment_bk(breakline_id,
                               triangle_id_1,
                               segment_id_1,
                               triangle_id_2,
                               segment_id_2)

    def insert_breakline(self, break_line_):
        """
        It manages the insertion of breakliness. Read data from disk, split
        breaklines into segments. Insert every point and corresponding
        :param break_line_: ogr.Geometry
        :return:
        """

        vertices_bk = break_line_.GetPoints()
        bk_id = str(int(self.get_max_id_breakline()) + 1)
        self.set_max_id_breakline(bk_id)
        self._breaklines[bk_id] = []

        # Insert the first point into the triangulation
        first_point = vertices_bk.pop(0)
        bk_ver = ogr.Geometry(ogr.wkbPoint)
        bk_ver.AddPoint(first_point[0],
                        first_point[1],
                        first_point[2])

        triangle_involved = self.search_triangle(bk_ver)
        if triangle_involved[1] is not None:
            ins_tri = self.insert_point_touches(bk_ver,
                                                triangle_involved[0],
                                                triangle_involved[1],
                                                self.get_max_id_triangle())
            self.set_max_id_triangle(ins_tri[3])
        else:
            ins_tri = self.insert_point_within(bk_ver,
                                               triangle_involved[0],
                                               self.get_max_id_triangle())
            self.set_max_id_triangle(ins_tri[2])
        point_before_geom = ogr.Geometry(ogr.wkbPoint)
        point_before_geom.AddPoint(bk_ver.GetX(),
                                   bk_ver.GetY(),
                                   bk_ver.GetZ())
        point_before_geom_2d = ogr.Geometry(ogr.wkbPoint)
        point_before_geom_2d.AddPoint(point_before_geom.GetX(),
                                      point_before_geom.GetY())
        point_before_geom_2d.FlattenTo2D()
        point_before_wkt_2d = point_before_geom_2d.ExportToWkt()
        point_before_wkt_3d = point_before_geom.ExportToWkt()

        segment_bk_vertex = ogr.Geometry(ogr.wkbPoint)
        segment_bk_vertex.AddPoint(bk_ver.GetX(),
                                   bk_ver.GetY(),
                                   bk_ver.GetZ())

        for point in vertices_bk:
            bk_ver = ogr.Geometry(ogr.wkbPoint)
            # Check if it is 3D data
            try:
                bk_ver.AddPoint(point[0], point[1], point[2])
            except IndexError:
                bk_ver.AddPoint(point[0], point[1])
            bk_ver_2d = ogr.Geometry(ogr.wkbPoint)
            bk_ver_2d.AddPoint(bk_ver.GetX(), bk_ver.GetY())
            bk_ver_2d.FlattenTo2D()
            bk_ver_wkt = bk_ver.ExportToWkt()
            bk_ver_wkt_2d = bk_ver_2d.ExportToWkt()

            # Get the segment_bk geom
            vir_seg = ogr.Geometry(ogr.wkbLineString)
            vir_seg.AddPoint(segment_bk_vertex.GetX(),
                             segment_bk_vertex.GetY(),
                             segment_bk_vertex.GetZ())
            vir_seg.AddPoint(bk_ver.GetX(),
                             bk_ver.GetY(),
                             bk_ver.GetZ())

            segment_bk_vertex = ogr.Geometry(ogr.wkbPoint)
            segment_bk_vertex.AddPoint(bk_ver.GetX(),
                                       bk_ver.GetY(),
                                       bk_ver.GetZ())

            is_vir_points_inserted = False
            while is_vir_points_inserted is False:
                # Get Intersection segment
                intersect_seg = ogr.Geometry(ogr.wkbLineString)
                intersect_seg.AddPoint(point_before_geom.GetX(),
                                       point_before_geom.GetY(),
                                       point_before_geom.GetZ())
                intersect_seg.AddPoint(bk_ver.GetX(),
                                       bk_ver.GetY(),
                                       bk_ver.GetZ())
                intersect_seg_2d = ogr.Geometry(ogr.wkbLineString)
                intersect_seg_2d.AddPoint_2D(intersect_seg.GetX(),
                                             intersect_seg.GetY())
                intersect_seg_2d.AddPoint_2D(bk_ver.GetX(),
                                             bk_ver.GetY())

                '''
                Search for virtual segment intersects any triangle. If the
                segment doesn't intersect, insert the vertex into triangulation.
                If the segment intersects, loop insertion of virtual segment 
                until no intersection is found.
                '''
                vir_point_found = False
                iterator = 0
                while (vir_point_found is False
                       and iterator < len(ins_tri)):
                    t_object = self.get_triangle_by_id(ins_tri[iterator])
                    segments_tri = t_object.get_segments()
                    # Loop the segments
                    for seg in segments_tri:
                        seg_geom_2d = seg.get_linestring_geometry()
                        seg_geom_2d.FlattenTo2D()
                        # Intersection tolerance may come up with Errors
                        intersection = intersect_seg_2d.Intersection(seg_geom_2d)
                        inters_wkt = intersection.ExportToWkt()
                        if (intersection.GetGeometryName() == "POINT"
                                and not inters_wkt == point_before_wkt_2d
                                and not inters_wkt == bk_ver_wkt_2d):
                            # Update z coordinate of bk_ver
                            v_p_geom = self.update_z_intersection_points([intersection],
                                                                         vir_seg)
                            v_p_wkt = v_p_geom[0].ExportToWkt()

                            # Insert the virtual point
                            ins_tri = self.insert_point_touches(
                                v_p_geom[0],
                                ins_tri[iterator],
                                seg.get_left_triangle(),
                                self.get_max_id_triangle())
                            self.set_max_id_triangle(ins_tri[3])

                            # Get triangles involves to update virtual segment
                            involved_triangles = list()
                            for i in range(0, 4):
                                tri_obj = self.get_triangle_by_id(ins_tri[i])
                                vertices = tri_obj.get_vertices_wkt()
                                if (point_before_wkt_3d in vertices
                                        and v_p_wkt in vertices):
                                    involved_triangles.append(ins_tri[i])

                            # Prepare variables for the next loop
                            self.update_segment_bk(involved_triangles[0],
                                                   involved_triangles[1],
                                                   bk_id)
                            point_before_geom = v_p_geom[0]
                            point_before_geom_2d = ogr.Geometry(ogr.wkbPoint)
                            point_before_geom_2d.AddPoint(point_before_geom.GetX(),
                                                          point_before_geom.GetY())
                            point_before_geom_2d.FlattenTo2D()
                            point_before_wkt_2d = point_before_geom_2d.ExportToWkt()
                            point_before_wkt_3d = point_before_geom.ExportToWkt()

                            # Break  intersection search
                            vir_point_found = True
                            break
                    iterator += 1

                if vir_point_found is True:
                    # A virtual point is found and another loop is required
                    pass
                else:
                    # No point found, end of virtual point insertion
                    is_vir_points_inserted = True

            '''
            Once the virtual points are inserted, the insertion bk_ver still
            remains
            '''
            # Get the triangle where the point is inside or touches
            triangle_involved = list()
            for tri in ins_tri:
                tri_obj = self.get_triangle_by_id(tri)
                tri_geom = tri_obj.get_geometry()
                if bk_ver.Within(tri_geom):
                    triangle_involved.append(tri)
                    triangle_involved.append(None)
                    break
                if bk_ver.Touches(tri_geom):
                    triangle_involved.append(tri)

            # Insert the virtual point
            if triangle_involved[1] is not None:
                ins_tri = self.insert_point_touches(bk_ver,
                                                    triangle_involved[0],
                                                    triangle_involved[1],
                                                    self.get_max_id_triangle())
                self.set_max_id_triangle(ins_tri[3])
            else:
                ins_tri = self.insert_point_within(bk_ver,
                                                   triangle_involved[0],
                                                   self.get_max_id_triangle())
                self.set_max_id_triangle(ins_tri[2])

            # Get triangles involves to update virtual segment
            involved_triangles = list()
            for tri in ins_tri:
                tri_obj = self.get_triangle_by_id(tri)
                vertices = tri_obj.get_vertices_wkt()
                if point_before_wkt_3d in vertices and bk_ver_wkt in vertices:
                    involved_triangles.append(tri)

            self.update_segment_bk(involved_triangles[0],
                                   involved_triangles[1],
                                   bk_id)
            # Update point_before
            point_before_geom = ogr.Geometry(ogr.wkbPoint)
            point_before_geom.AddPoint(bk_ver.GetX(),
                                       bk_ver.GetY(),
                                       bk_ver.GetZ())
            point_before_geom_2d = ogr.Geometry(ogr.wkbPoint)
            point_before_geom_2d.AddPoint_2D(point_before_geom.GetX(),
                                             point_before_geom.GetY())
            point_before_wkt_2d = point_before_geom_2d.ExportToWkt()
            point_before_wkt_3d = point_before_geom.ExportToWkt()

    def update_segment_hu(self,
                          triangle_id_1,
                          triangle_id_2,
                          hull_id):
        """
        Given two triangles and vertices of a segment, find the segment to
        update. Also works with Hulls. Update triangle object directly. Be sure
        to assign correct values to breakline_id. Be sure that triangle_id_1 and
        triangle_id_2 are together. Appends a new entry into breakline
        attribute. To call the segment, it is used the index in the breakline
        list.
        :param triangle_id_1: String ID triangle
        :param triangle_id_2: String ID triangle
        :param hull_id: String ID of breakline
        """

        t1 = self.get_triangle_by_id(triangle_id_1)
        t2 = self.get_triangle_by_id(triangle_id_2)
        t1_vertices = t1.get_vertices_wkt()
        t2_vertices = t2.get_vertices_wkt()
        segments = t1.get_segments()
        segment_index = len(self.get_hull_by_id(hull_id))
        segment_id_1 = None
        segment_id_2 = None

        # Find and update common segment in t1 triangle
        for i in range(0, 3):
            seg = segments[i]
            start_p_seg = seg.get_start_point()
            start_p_seg_wkt = start_p_seg.ExportToWkt()
            end_p_seg = seg.get_end_point()
            end_p_seg_wkt = end_p_seg.ExportToWkt()
            if (start_p_seg_wkt in t2_vertices
                    and end_p_seg_wkt in t2_vertices):
                # Set triangle's segment information
                seg.set_hull(str(hull_id), str(segment_index))
                segments[i] = seg
                segment_id_1 = i
                break
        t1.set_segments_by_list(segments)
        self._triangles[triangle_id_1] = t1

        # Find and update common segment in t2 triangle
        segments = t2.get_segments()
        for i in range(0, 3):
            seg = segments[i]
            start_p_seg = seg.get_start_point()
            start_p_seg_wkt = start_p_seg.ExportToWkt()
            end_p_seg = seg.get_end_point()
            end_p_seg_wkt = end_p_seg.ExportToWkt()
            if (start_p_seg_wkt in t1_vertices
                    and end_p_seg_wkt in t1_vertices):
                # Set triangle's segment information
                seg.set_hull(str(hull_id), str(segment_index))
                segments[i] = seg
                segment_id_2 = i
                break
        t2.set_segments_by_list(segments)
        self._triangles[triangle_id_2] = t2

        # Set breakline attribute information
        self.append_segment_hu(hull_id,
                               triangle_id_1,
                               segment_id_1,
                               triangle_id_2,
                               segment_id_2)

    def insert_hulls(self, hull_):
        """
        It manages the insertion of hulls. Read data from disk, split
        breaklines into segments. Insert every point and corresponding
        :param hull_: ogr.Geometry
        :return:
        """

        ring_hu = hull_.GetGeometryRef(0)
        vertices_hu = []
        for i in range(0, ring_hu.GetPointCount()):
            pt = ring_hu.GetPoint(i)
            vertices_hu.append(pt)

        hu_id = str(int(self.get_max_id_hull()) + 1)
        self.set_max_id_hull(hu_id)
        self._hulls[hu_id] = [hull_]

        # Insert the first point into the triangulation
        first_point = vertices_hu.pop(0)
        hu_ver = ogr.Geometry(ogr.wkbPoint)
        hu_ver.AddPoint(first_point[0],
                        first_point[1],
                        first_point[2])
        first_point = ogr.Geometry(ogr.wkbPoint)
        first_point.AddPoint_2D(hu_ver.GetX(),
                                hu_ver.GetY())
        first_geom_wkt = first_point.ExportToWkt()

        triangle_involved = self.search_triangle(hu_ver)
        if triangle_involved[1] is not None:
            ins_tri = self.insert_point_touches(hu_ver,
                                                triangle_involved[0],
                                                triangle_involved[1],
                                                self.get_max_id_triangle())
            self.set_max_id_triangle(ins_tri[3])
        else:
            ins_tri = self.insert_point_within(hu_ver,
                                               triangle_involved[0],
                                               self.get_max_id_triangle())
            self.set_max_id_triangle(ins_tri[2])
        point_before_geom = ogr.Geometry(ogr.wkbPoint)
        point_before_geom.AddPoint(hu_ver.GetX(),
                                   hu_ver.GetY(),
                                   hu_ver.GetZ())
        point_before_geom_2d = ogr.Geometry(ogr.wkbPoint)
        point_before_geom_2d.AddPoint_2D(point_before_geom.GetX(),
                                         point_before_geom.GetY(),)
        point_before_wkt_2d = point_before_geom_2d.ExportToWkt()
        point_before_wkt_3d = point_before_geom.ExportToWkt()

        segment_hu_vertex = ogr.Geometry(ogr.wkbPoint)
        segment_hu_vertex.AddPoint(hu_ver.GetX(),
                                   hu_ver.GetY(),
                                   hu_ver.GetZ())

        for point in vertices_hu:
            hu_ver = ogr.Geometry(ogr.wkbPoint)
            # Check if it is 3D data
            if len(point) == 3:
                hu_ver.AddPoint(point[0], point[1], point[2])
            else:
                hu_ver.AddPoint(point[0], point[1])
            hu_ver_2d = ogr.Geometry(ogr.wkbPoint)
            hu_ver_2d.AddPoint_2D(hu_ver.GetX(),
                                  hu_ver.GetY())
            hu_ver_wkt = hu_ver.ExportToWkt()
            hu_ver_wkt_2d = hu_ver_2d.ExportToWkt()

            # Get the segment_hu geom
            vir_seg = ogr.Geometry(ogr.wkbLineString)
            vir_seg.AddPoint(segment_hu_vertex.GetX(),
                             segment_hu_vertex.GetY(),
                             segment_hu_vertex.GetZ())
            vir_seg.AddPoint(hu_ver.GetX(),
                             hu_ver.GetY(),
                             hu_ver.GetZ())
            segment_hu_vertex = ogr.Geometry(ogr.wkbPoint)
            segment_hu_vertex.AddPoint(hu_ver.GetX(),
                                       hu_ver.GetY(),
                                       hu_ver.GetZ())

            is_vir_points_inserted = False
            while is_vir_points_inserted is False:
                # Get Intersection segment
                intersect_seg = ogr.Geometry(ogr.wkbLineString)
                intersect_seg.AddPoint(point_before_geom.GetX(),
                                       point_before_geom.GetY(),
                                       point_before_geom.GetZ())
                intersect_seg.AddPoint(hu_ver.GetX(),
                                       hu_ver.GetY(),
                                       hu_ver.GetZ())
                intersect_seg_2d = ogr.Geometry(ogr.wkbLineString)
                intersect_seg_2d.AddPoint_2D(point_before_geom.GetX(),
                                             point_before_geom.GetY())
                intersect_seg_2d.AddPoint_2D(hu_ver.GetX(),
                                             hu_ver.GetY())

                '''
                Search for virtual segment intersects any triangle. If the
                segment doesn't intersect, insert the vertex into triangulation.
                If the segment intersects, loop insertion of virtual segment 
                until no intersection is found.
                '''
                vir_point_found = False
                iterator = 0
                while (vir_point_found is False
                       and iterator < len(ins_tri)):
                    t_object = self.get_triangle_by_id(ins_tri[iterator])
                    segments_tri = t_object.get_segments()
                    # Loop the segments
                    for seg in segments_tri:
                        seg_geom = seg.get_linestring_geometry()
                        seg_geom_2d = ogr.Geometry(ogr.wkbLineString)
                        seg_geom_2d.AddPoint_2D(seg_geom.GetPoint(0)[0],
                                                seg_geom.GetPoint(0)[1])
                        seg_geom_2d.AddPoint_2D(seg_geom.GetPoint(1)[0],
                                                seg_geom.GetPoint(1)[1])

                        # Intersection tolerance may come up with Errors
                        intersection = intersect_seg_2d.Intersection(seg_geom_2d)
                        inters_wkt = intersection.ExportToWkt()
                        if (intersection.GetGeometryName() == "POINT"
                                and not inters_wkt == point_before_wkt_2d
                                and not inters_wkt == hu_ver_wkt_2d):
                            # Update z coordinate of bk_ver
                            v_p_geom = self.update_z_intersection_points([intersection],
                                                                         vir_seg)
                            v_p_wkt = v_p_geom[0].ExportToWkt()

                            # Insert the virtual point
                            ins_tri = self.insert_point_touches(
                                v_p_geom[0],
                                ins_tri[iterator],
                                seg.get_left_triangle(),
                                self.get_max_id_triangle())
                            self.set_max_id_triangle(ins_tri[3])

                            # Get triangles involves to update virtual segment
                            involved_triangles = list()
                            for i in range(0, 4):
                                tri_obj = self.get_triangle_by_id(ins_tri[i])
                                vertices = tri_obj.get_vertices_wkt()
                                if (point_before_wkt_3d in vertices
                                        and v_p_wkt in vertices):
                                    involved_triangles.append(ins_tri[i])

                            # Prepare variables for the next loop
                            self.update_segment_hu(involved_triangles[0],
                                                   involved_triangles[1],
                                                   hu_id)
                            point_before_geom = ogr.Geometry(ogr.wkbPoint)
                            point_before_geom.AddPoint(v_p_geom[0].GetX(),
                                                       v_p_geom[0].GetY(),
                                                       v_p_geom[0].GetZ())
                            point_before_geom_2d = ogr.Geometry(ogr.wkbPoint)
                            point_before_geom_2d.AddPoint_2D(v_p_geom[0].GetX(),
                                                             v_p_geom[0].GetY())
                            point_before_wkt_2d = point_before_geom_2d.ExportToWkt()
                            point_before_wkt_3d = point_before_geom.ExportToWkt()

                            # Break  intersection search
                            vir_point_found = True
                            break
                    iterator += 1

                if vir_point_found is True:
                    # A virtual point is found and another loop is required
                    pass
                else:
                    # No point found, end of virtual point insertion
                    is_vir_points_inserted = True

            '''
            Once the virtual points are inserted, the insertion hu_ver still
            remains
            '''

            # Get the triangle where the point is inside or touches
            triangle_involved = list()
            for tri in ins_tri:
                tri_obj = self.get_triangle_by_id(tri)
                tri_geom = tri_obj.get_geometry()
                if hu_ver.Within(tri_geom):
                    triangle_involved.append(tri)
                    triangle_involved.append(None)
                    break
                if hu_ver.Touches(tri_geom):
                    triangle_involved.append(tri)

            if first_geom_wkt != hu_ver_wkt_2d:
                # Insert the virtual point
                if triangle_involved[1] is not None:
                    ins_tri = self.insert_point_touches(hu_ver,
                                                        triangle_involved[0],
                                                        triangle_involved[1],
                                                        self.get_max_id_triangle())
                    self.set_max_id_triangle(ins_tri[3])
                else:
                    ins_tri = self.insert_point_within(hu_ver,
                                                       triangle_involved[0],
                                                       self.get_max_id_triangle())
                    self.set_max_id_triangle(ins_tri[2])

            # Get triangles involves to update virtual segment
            involved_triangles = list()
            for tri in ins_tri:
                tri_obj = self.get_triangle_by_id(tri)
                vertices = tri_obj.get_vertices_wkt()
                if point_before_wkt_3d in vertices and hu_ver_wkt in vertices:
                    involved_triangles.append(tri)

            self.update_segment_hu(involved_triangles[0],
                                   involved_triangles[1],
                                   hu_id)
            # Update point_before
            point_before_geom = ogr.Geometry(ogr.wkbPoint)
            point_before_geom.AddPoint(hu_ver.GetX(),
                                       hu_ver.GetY(),
                                       hu_ver.GetZ())
            point_before_geom_2d = ogr.Geometry(ogr.wkbPoint)
            point_before_geom_2d.AddPoint_2D(hu_ver.GetX(),
                                             hu_ver.GetY())
            point_before_wkt_2d = point_before_geom_2d.ExportToWkt()
            point_before_wkt_3d = point_before_geom.ExportToWkt()

    def toggle_visibility_ficticial_triangles(self):
        """
        Toggle the visibility of ficticial triangles
        """
        ficticial_points = self._ficticial_points
        for tri in list(self._triangles.keys()):
            tri_object = self.get_triangle_by_id(tri)
            vertices = tri_object.get_vertices_wkt()
            if (vertices[0] in ficticial_points
                    or vertices[1] in ficticial_points
                    or vertices[2] in ficticial_points):
                tri_object.hide_visibility()
                self._triangles[tri] = tri_object

    def toggle_visibility_triangles_hull(self):
        """
        Toggle the visibility of triangles inside hulls
        """

        temporal_set = set()
        for key in list(self._hulls.keys()):
            hull = self._hulls[key]
            geom = hull.pop(0)
            area_geom = geom.Area()
            area_geom = area_geom + area_geom * 0.0001
            seg_hu = hull.pop(0)
            tri_object = self.get_triangle_by_id(seg_hu[0])
            tri_geom = tri_object.get_geometry()
            geom_union = tri_geom.Union(geom)
            if geom_union.Area() > area_geom:
                # The triangle is out of hull
                temporal_set.add(seg_hu[2])
            else:
                # The triangle is inside hull
                temporal_set.add(seg_hu[0])
            global_set = temporal_set

            while len(temporal_set) != 0:
                # Determine adjacent triangles
                temporal_set2 = set()
                for tri in temporal_set:
                    tri_object = self.get_triangle_by_id(tri)
                    adj_tri = tri_object.get_adjacent_triangles()
                    adj_tri = set(adj_tri)
                    temporal_set2 = temporal_set2 | adj_tri
                temporal_set = temporal_set2

                # Take only those new ones
                temporal_set = temporal_set - global_set

                # Take only the ones inside hull
                temporal_set2 = set()
                for tri in temporal_set:
                    tri_object = self.get_triangle_by_id(tri)
                    tri_geom = tri_object.get_geometry()
                    geom_union = tri_geom.Union(geom)
                    if geom_union.Area() > area_geom:
                        # The triangle is out of hull
                        pass
                    else:
                        # The triangle is inside hull
                        temporal_set2.add(tri)
                temporal_set = temporal_set2

                global_set = global_set | temporal_set

            # Toggle Visibility
            for tri in global_set:
                tri_object = self.get_triangle_by_id(tri)
                if tri_object.get_visibility():
                    tri_object.hide_visibility()
                    self._triangles[tri] = tri_object

    def update_hull(self,
                    key_hull,
                    key_segment,
                    old_triangle_key,
                    new_triangle_key):

        """
        Updates the attribute self._hull with the properly content, that changes
        when new points are inserted into the mesh
        """

        # Find the triangle to change
        content = self.get_hull_by_id(key_hull)[int(key_segment)]
        triangle_index = content.index(old_triangle_key)
        if triangle_index == 0:
            self._hulls[key_hull][int(key_segment)] = [new_triangle_key,
                                                       1,
                                                       content[2],
                                                       content[3]]
        else:
            self._hulls[key_hull][int(key_segment)] = [content[0],
                                                       content[1],
                                                       new_triangle_key,
                                                       1]

    def update_breakline(self,
                         key_breakline,
                         key_segment,
                         old_triangle_key,
                         new_triangle_key):
        """
        Updates the attribute self._breakline with the properly content, that changes
        when new points are inserted into the mesh
        """

        # Find the triangle to change
        content = self.get_breakline_by_id(key_breakline)[int(key_segment)]
        triangle_index = content.index(old_triangle_key)
        if triangle_index == 0:
            self._breaklines[key_breakline][int(key_segment)] = [new_triangle_key,
                                                                 1,
                                                                 content[2],
                                                                 content[3]]
        else:
            self._breaklines[key_breakline][int(key_segment)] = [content[0],
                                                                 content[1],
                                                                 new_triangle_key,
                                                                 1]

    def export_triangles_to_shp(self, out_path, name_layer):
        """
        Write the triangulation to the disk
        :param out_path: String
        :param name_layer: String
        :return:
        """

        # set up the shapefile driver
        driver = ogr.GetDriverByName('Esri Shapefile')

        # create the data source
        extension = ".shp"
        data_source = driver.CreateDataSource(out_path + os.path.sep + name_layer + extension)

        '''
        # create the spatial reference, EPSG:25830
        srs = ogs.SpatialReference()
        srs.ImportFromEPSG(25830)
        '''

        # Remove file if exists
        output_file = os.path.join(out_path, name_layer)
        if os.path.exists(output_file + extension):
            driver.DeleteDataSource(output_file + extension)

        # create the layer
        layer = data_source.CreateLayer(name_layer,
                                        None,
                                        ogr.wkbPolygon25D)

        # Add fields
        field_name = ogr.FieldDefn("id", ogr.OFTString)
        field_name.SetWidth(24)
        layer.CreateField(field_name)

        # Process the data
        for triangle_ in list(self._triangles.values()):
            if triangle_.get_visibility():
                # Create the feature
                feature = ogr.Feature(layer.GetLayerDefn())

                # Set the attributes using the values from triangle object
                feature.SetField("id", triangle_.get_id())

                # Set the feature geometry
                feature.SetGeometry(triangle_.get_geometry())

                # Create the feature in the layer (shapefile)
                layer.CreateFeature(feature)

                # Dereference the feature
                feature = None

        # Save and close the data source
        data_source.FlushCache()
        data_source = None

    def export_segments_to_shp(self, out_path, name_layer):
        """
        Write the triangulation segments to the disk
        :param out_path: String
        :param name_layer: String
        :return:
        """

        # set up the shapefile driver
        driver = ogr.GetDriverByName('Esri Shapefile')

        # create the data source
        extension = ".shp"
        data_source = driver.CreateDataSource(out_path + os.path.sep + name_layer + extension)

        '''
        # create the spatial reference, EPSG:25830
        srs = ogs.SpatialReference()
        srs.ImportFromEPSG(25830)
        '''

        # Remove file if exists
        output_file = os.path.join(out_path, name_layer)
        if os.path.exists(output_file + extension):
            driver.DeleteDataSource(output_file + extension)

        # create the layer
        layer = data_source.CreateLayer(name_layer,
                                        None,
                                        ogr.wkbLineString)

        # Add fields
        field_name = ogr.FieldDefn("id", ogr.OFTString)
        field_name.SetWidth(24)
        layer.CreateField(field_name)
        layer.CreateField(ogr.FieldDefn("left_trian", ogr.OFTString))
        layer.CreateField(ogr.FieldDefn("geometry", ogr.OFTString))
        layer.CreateField(ogr.FieldDefn("hull", ogr.OFTString))
        layer.CreateField(ogr.FieldDefn("hull1", ogr.OFTString))
        layer.CreateField(ogr.FieldDefn("breakline", ogr.OFTString))
        layer.CreateField(ogr.FieldDefn("breakline1", ogr.OFTString))

        # Process the data
        for triangle_ in list(self._triangles.values()):
            # Create the feature segment_a
            feature = ogr.Feature(layer.GetLayerDefn())

            # Set the attributes using the values from triangle object
            feature.SetField("id", triangle_.get_id())
            segment_a = triangle_.get_segment_a()
            left_triangle_segment_a = segment_a.get_left_triangle()
            feature.SetField("left_trian", left_triangle_segment_a)
            feature.SetField("geometry",
                             segment_a.get_linestring_geometry().ExportToWkt())
            feature.SetField("hull", segment_a.get_hull())
            feature.SetField("hull1", segment_a.get_hull_segment())
            feature.SetField("breakline", segment_a.get_breakline())
            feature.SetField("breakline1",
                             segment_a.get_breakline_segment())

            # Set the feature geometry
            feature.SetGeometry(segment_a.get_linestring_geometry())

            # Create the feature in the layer (shapefile)
            layer.CreateFeature(feature)

            # Dereference the feature
            feature = None

            # Create the feature segment_b
            feature = ogr.Feature(layer.GetLayerDefn())

            # Set the attributes using the values from triangle object
            feature.SetField("id", triangle_.get_id())
            segment_b = triangle_.get_segment_b()
            feature.SetField("left_trian", segment_b.get_left_triangle())
            feature.SetField("geometry",
                             segment_b.get_linestring_geometry().ExportToWkt())
            feature.SetField("hull", segment_b.get_hull())
            feature.SetField("hull1", segment_b.get_hull_segment())
            feature.SetField("breakline", segment_b.get_breakline())
            feature.SetField("breakline1",
                             segment_b.get_breakline_segment())

            # Set the feature geometry
            feature.SetGeometry(segment_b.get_linestring_geometry())

            # Create the feature in the layer (shapefile)
            layer.CreateFeature(feature)

            # Dereference the feature
            feature = None

            # Create the feature segment_c
            feature = ogr.Feature(layer.GetLayerDefn())

            # Set the attributes using the values from triangle object
            feature.SetField("id", triangle_.get_id())
            segment_c = triangle_.get_segment_c()
            feature.SetField("left_trian", segment_c.get_left_triangle())
            feature.SetField("geometry",
                             segment_c.get_linestring_geometry().ExportToWkt())
            feature.SetField("hull", segment_c.get_hull())
            feature.SetField("hull1", segment_c.get_hull_segment())
            feature.SetField("breakline", segment_c.get_breakline())
            feature.SetField("breakline1",
                             segment_c.get_breakline_segment())

            # Set the feature geometry
            feature.SetGeometry(segment_c.get_linestring_geometry())

            # Create the feature in the layer (shapefile)
            layer.CreateFeature(feature)

            # Dereference the feature
            feature = None

        # Save and close the data source
        data_source.FlushCache()
        data_source = None

    def triangulate(self):
        """Insert each point of layer points into the mesh"""

        for point_ in self._features_points:
            # print self.__str__()
            triangle_involved = self.search_triangle(point_.GetGeometryRef())

            '''
            # Export data before executing the insertion.
            debug_path = "/tmp/debug/inserting_iterations"
            triangle_layer = "%s_triangle_involved_%s" % (iteration,
                                                          triangle_involved[0])
            segment_layer = "%s_segments_involved_%s" % (iteration,
                                                         triangle_involved[0])
            self.export_triangles_to_shp(debug_path, triangle_layer)
            self.export_segments_to_shp(debug_path, segment_layer)
            iteration += 1
            '''
            # Create and remove the triangles needed to insert the point
            if triangle_involved[1] is not None:
                content = self.insert_point_touches(point_.GetGeometryRef(),
                                                    triangle_involved[0],
                                                    triangle_involved[1],
                                                    self.get_max_id_triangle())
                self.set_max_id_triangle(content[3])

                # Evaluation of Delaunay Condition
                check_list_ = self.create_check_list(content,
                                                     self.get_adjacency())
                self.check_list(check_list_, self.get_adjacency())

            else:
                content = self.insert_point_within(point_.GetGeometryRef(),
                                                   triangle_involved[0],
                                                   self.get_max_id_triangle())
                self.set_max_id_triangle(content[2])

                # Evaluation of Delaunay Condition
                check_list_ = self.create_check_list(content,
                                                     self.get_adjacency())
                self.check_list(check_list_, self.get_adjacency())

        for break_line in self._features_breaklines:
            self.insert_breakline(break_line.GetGeometryRef())
        for hull in self._features_hulls:
            self.insert_hulls(hull.GetGeometryRef())
        self.toggle_visibility_triangles_hull()
        self.toggle_visibility_ficticial_triangles()

        # Output
        output_triangles_path = os.path.split(self._output_triangles)
        self.export_triangles_to_shp(output_triangles_path[0], output_triangles_path[1])
        output_segments_path = os.path.split(self._output_segments)
        self.export_segments_to_shp(output_segments_path[0], output_segments_path[1])

    def __str__(self):
        multipolygon = ogr.Geometry(ogr.wkbMultiPolygon)
        text = "TRIANGULATION CONTENT\n"
        for triangle_ in list(self._triangles.values()):
            multipolygon.AddGeometry(triangle_.get_geometry())
            text = (text
                    + "Triangle_Id: %s" % triangle_.get_id()
                    + " WKT: %s\n" % triangle_.get_geometry().ExportToWkt())
        text = text + multipolygon.ExportToWkt() + "\n"
        text = text + "TRIANGULATION CONTENT END"

        return text
