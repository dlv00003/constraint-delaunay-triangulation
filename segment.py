# -*- coding: utf-8 -*-

"""
File: Segment.py
This class gives all necesary resources for dealing with segments of triangles
"""
import osgeo.ogr as ogr
ogr.UseExceptions()


class Segment(ogr.Geometry):

        def __init__(self,
                     start_point,
                     end_point,
                     left_triangle,
                     breakline,
                     hull):
            """
            Constructor creates a Segment with its Start Point, EndPoint and
            LeftTriangle. Each Segment must belong to a triangle; all Segments
            in a triangle are clock wise oriented, that is why it is not needs
            to know the Right Triangle.
            :param start_point: ogr.Geometry
            :param end_point: ogr.Geometry
            :param left_triangle: String
            :param breakline: String Example: 1_3 it corresponds to breakline 1,
            segment of breakline 3. It can be None
            :param hull: String Example: 3_5 it corresponds to hull 1, segment
            of hull 5. It can be None
            """

            ogr.Geometry.__init__(self, ogr.wkbPoint)
            self._StartPoint = start_point
            self._EndPoint = end_point
            self._LeftTriangle = left_triangle
            self._breakline = breakline
            self._hull = hull

        def get_start_point(self):
            """Returns the starting point of the segment"""
            return self._StartPoint

        def get_end_point(self):
            """Returns the ending point of the segment"""
            return self._EndPoint

        def get_left_triangle(self):
            """Returns the left side triangle of the segment"""
            return self._LeftTriangle

        def get_breakline(self):
            """Returns the breakline id that segment belongs to"""
            if self._breakline is None:
                return None
            else:
                breakline_id = self._breakline.split("_")
                return breakline_id[0]

        def get_breakline_segment(self):
            """Returns the segment id of the breakline"""
            if self._breakline is None:
                return None
            else:
                segment_breakline_id = self._breakline.split("_")
                return segment_breakline_id[1]

        def get_hull(self):
            """Returns the breakline id that segment belongs to"""
            if self._hull is None:
                return None
            else:
                hull_id = self._hull.split("_")
                return hull_id[0]

        def get_hull_segment(self):
            """Returns the segment id of the breakline"""
            if self._hull is None:
                return None
            else:
                segment_hull_id = self._hull.split("_")
                return segment_hull_id[1]

        def get_linestring_geometry(self):
            """Returns the segment as ogr.wkbLineString"""
            geometry = ogr.Geometry(ogr.wkbLineString)
            geometry.AddPoint(self._StartPoint.GetX(),
                              self._StartPoint.GetY(),
                              self._StartPoint.GetZ())
            geometry.AddPoint(self._EndPoint.GetX(),
                              self._EndPoint.GetY(),
                              self._EndPoint.GetZ())

            return geometry

        def set_left_triangle(self, triangle_key):
            """Stablish the left triangle of the Segment"""
            self._LeftTriangle = triangle_key

        def set_breakline(self, breakline_, segment_):
            """
            Stablish the breakline that the segment belongs to and set which
            segment of the breakline corresponds to this segment.
            :param breakline_: String ID
            :param segment_: String ID
            """
            self._breakline = breakline_ + "_" + segment_

        def set_hull(self, hull_, segment_):
            """
            Stablish the hull that the segment belongs to and set which
            segment of the hull corresponds to this segment.
            :param hull_: String ID
            :param segment_: String ID
            """
            self._hull = hull_ + "_" + segment_

        def is_segment_aside(self, oposite_segment):
            """
            Compare starting and ending vertices of both segments
            :param oposite_segment: segment.Segment
            :return: True if oposite segment is alongside to segment
            """

            s1_wkt = self.get_start_point().ExportToWkt()
            s2_wkt = self.get_end_point().ExportToWkt()
            o1_wkt = oposite_segment.get_start_point().ExportToWkt()
            o2_wkt = oposite_segment.get_end_point().ExportToWkt()

            if s1_wkt == o2_wkt and s2_wkt == o1_wkt:
                return True
            else:
                return False

        def is_point_left_side(self, point):
            """
            Determine if a point is in the left side of the line, or it is
            the other way arround.
            :param point: ogr.Geometry(ogr.wkbPoint)
            :return: 0 point is colineal, 1 is to the right, 2 is to the left
            """

            if point.GetGeometryName().lower() == "point":
                pass
            else:
                raise RuntimeError("You need to use ogr.wkbPoint as parameter"
                                   + ". Used %s" % (point.GetGeometryName()))

            '''
            Determinant determines if a point is to the left or to the right of
            segment
            '''

            a = [self.get_start_point().GetX(),
                 self.get_start_point().GetY()]
            b = [self.get_end_point().GetX(),
                 self.get_end_point().GetY()]
            c = [point.GetX(), point.GetY()]
            triangle_area_2 = (a[0] * b[1] - a[1] * b[0]
                               + b[0] * c[1] - b[1] * c[0]
                               + c[0] * a[1] - c[1] * a[0])

            if triangle_area_2 > 0:
                return 2  # Positive area, point is to the left of segment
            elif triangle_area_2 == 0:
                return 0  # Null area, point belongs to the line
            else:
                return 1  # Negative area, point is to the right of segment

        def __str__(self):
            text = ("Starting Point: %s " % self.get_start_point().ExportToWkt()
                    + "Ending Point: %s " % self.get_end_point().ExportToWkt()
                    + "Left Triangle Id: %s " % self.get_left_triangle()
                    + "Breakline ID: %s " % self.get_breakline()
                    + "Segment_Breakline Id: %s " % self.get_breakline_segment()
                    + "Hull Id: %s " % self.get_hull()
                    + "Segment_Hull_Id: %s " % self.get_hull_segment())
            return text
