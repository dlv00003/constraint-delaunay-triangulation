# -*- coding: utf-8 -*-

"""
File: triangle.py
This class gives all necesary resources for dealing with triangles
"""


from . import segment
import osgeo.ogr as ogr
ogr.UseExceptions()


class Triangle:

    def __init__(self, id_, segment_a, segment_b, segment_c):
        """
        Creates a triangle with its three segments, these segments can be
        Segments or Segment_Hull or Segment_Breakline.
        :param id_: String Name of triangle
        :param segment_a: segment.Segment
        :param segment_b: segment.Segment
        :param segment_c: segment.Segment
        """

        # Check that segments are segment objects
        if type(id_) == str:
            self._id = id_
        else:
            raise "Given %s object. Expected a string object" % (str(type(id)))

        if type(segment_a) == segment.Segment:
            self._segment_a = segment_a
        else:
            raise "Given %s object. " % str(type(segment_a))

        if type(segment_b) == segment.Segment:
            self._segment_b = segment_b
        else:
            raise "Given %s object. " % str(type(segment_b))

        if type(segment_c) == segment.Segment:
            self._segment_c = segment_c
        else:
            raise "Given %s object. " % str(type(segment_c))

        self._visibility = True

        # Build the geometry
        self._geometry = ogr.Geometry(ogr.wkbPolygon)
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(self.get_segment_b().get_start_point().GetX(),
                      self.get_segment_b().get_start_point().GetY(),
                      self.get_segment_b().get_start_point().GetZ())
        ring.AddPoint(self.get_segment_b().get_end_point().GetX(),
                      self.get_segment_b().get_end_point().GetY(),
                      self.get_segment_b().get_end_point().GetZ())
        ring.AddPoint(self.get_segment_c().get_end_point().GetX(),
                      self.get_segment_c().get_end_point().GetY(),
                      self.get_segment_c().get_end_point().GetZ())
        ring.CloseRings()
        self._geometry.AddGeometry(ring)

    def get_id(self):
        """Returns the triangle ID"""
        return self._id

    def get_segment_a(self):
        """Returns the first segment"""
        return self._segment_a

    def get_segment_b(self):
        """Returns the second segment"""
        return self._segment_b

    def get_segment_c(self):
        """Return the third segment"""
        return self._segment_c

    def get_visibility(self):
        """Return True if triangle can be drawn"""
        return self._visibility

    def is_valid(self):
        """Check geometry validation"""
        wkt_a_start = self.get_segment_a().get_start_point()
        wkt_a_end = self.get_segment_a().get_end_point()
        wkt_b_start = self.get_segment_b().get_start_point()
        wkt_b_end = self.get_segment_b().get_end_point()
        wkt_c_start = self.get_segment_c().get_start_point()
        wkt_c_end = self.get_segment_c().get_end_point()
        if wkt_a_end != wkt_b_start:
            raise RuntimeError("Building the triangle %s: Ending " % self._id
                               + "point of segment_a doesn't correspond to "
                                 "starting point of segment_b")
        if wkt_b_end != wkt_c_start:
            raise RuntimeError("Building the triangle %s: Ending " % self._id
                               + "point of segment_b doesn't correspond to "
                                 "starting point of segment_c")
        if wkt_c_end != wkt_a_start:
            raise RuntimeError("Building the triangle %s: Ending " % self._id
                               + "point of segment_c doesn't correspond to "
                                 "starting point of segment_a")

    def get_geometry(self):
        """Return a wkbPolygon geometry of the triangle"""
        return self._geometry

    def get_ring_triangle(self):
        """Return the ogr.wkblinearing version of the triangle"""
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddGeometry(self._segment_a.get_start_point().GetX(),
                         self._segment_a.get_start_point().GetY())
        ring.AddGeometry(self._segment_b.get_start_point().GetX(),
                         self._segment_b.get_start_point().GetY())
        ring.AddGeometry(self._segment_c.get_start_point().GetX(),
                         self._segment_c.get_start_point().GetY())
        return ring

    def get_vertices(self):
        """
        Return a list of ogr.wkbPoint corresponding to vertices of
        triangle. The first vertex corresponds to start point of segment A, the
        second vertex corresponds to end point of segment A and the third vertex
        corresponds to end point of segment B
        :return vertices_: list of vertices
        """
        vertices_ = list()
        vertices_.append(self.get_segment_a().get_start_point())
        vertices_.append(self.get_segment_a().get_end_point())
        vertices_.append(self.get_segment_b().get_end_point())
        return vertices_

    def get_vertices_wkt(self):
        """
        Return a list of wkt that correspond to triangle's vertices
        :return vertices_wkt: list of wkt version of vertices
        """
        vertices_wkt = list()
        vertex_a = self.get_segment_a().get_start_point()
        vertices_wkt.append(vertex_a.ExportToWkt())
        vertex_b = self.get_segment_a().get_end_point()
        vertices_wkt.append(vertex_b.ExportToWkt())
        vertex_c = self.get_segment_b().get_end_point()
        vertices_wkt.append(vertex_c.ExportToWkt())
        return vertices_wkt

    def get_segments(self):
        """
        Return a list of triangle's segments
        :return: list of segment.Segment
        """
        return [self._segment_a, self._segment_b, self._segment_c]

    def set_segments_by_list(self, list_):
        """
        Given a list of segment objects, modify the current triangle
        :param list_: list of segment.Segment
        """
        self.set_segment_a(list_[0])
        self.set_segment_b(list_[1])
        self.set_segment_c(list_[2])

    def get_adjacent_triangles(self):
        """Return a list of adjacent triangles"""
        adjacent_triangles = list()
        adjacent_triangles.append(self.get_segment_a().get_left_triangle())
        adjacent_triangles.append(self.get_segment_b().get_left_triangle())
        adjacent_triangles.append(self.get_segment_c().get_left_triangle())
        return adjacent_triangles

    def set_segment_a(self, segment_a_):
        """Set the segment a"""
        self._segment_a = segment_a_

    def set_segment_b(self, segment_b_):
        """Set the segment b"""
        self._segment_b = segment_b_

    def set_segment_c(self, segment_c_):
        """Set the segment c"""
        self._segment_c = segment_c_

    def toggle_visibility(self):
        """Change the visibility attribute from True to False or viceversa"""
        if self._visibility is True:
            self._visibility = False
        else:
            self._visibility = True

    def hide_visibility(self):
        """Change the visibility attribute from True to False"""
        self._visibility = False

    def next_segment(self, segment_):
        """
        Given a segment of the current triangle, returns the next clock wise
        segment
        :param segment_: segment.Segment
        :return: segment.Segment
        """

        # Discard the input segment
        segments = []
        if self.get_segment_a() != segment_:
            segments.append(self.get_segment_a())
        if self.get_segment_b() != segment_:
            segments.append(self.get_segment_b())
        if self.get_segment_c() != segment_:
            segments.append(self.get_segment_c())

        # Select the segment
        if segment_.get_end_point() == segments[0].get_start_point():
            return segments[0]
        else:
            return segments[1]

    def get_circunscrit_circle(self):
        """
        It determines the cincunscrit circle of the triangle
        Returns a buffer which center correspond to center of the circle
        and radio of buffer corresponds to radio of circle
        :return: ogr.Geometry
        """

        polygon_geometry = self.get_geometry()
        ring_geometry = polygon_geometry.GetGeometryRef(0)
        p1 = ring_geometry.GetPoint(0)
        x1 = p1[0]
        y1 = p1[1]
        x2 = ring_geometry.GetPoint(1)[0]
        y2 = ring_geometry.GetPoint(1)[1]
        x3 = ring_geometry.GetPoint(2)[0]
        y3 = ring_geometry.GetPoint(2)[1]

        # Determine middle point segment s1:(p2, p1)
        x5 = x1 + (x2 - x1) * 0.5
        y5 = y1 + (y2 - y1) * 0.5

        # Determine middle point segment s2:(p3, p2)
        x6 = x2 + (x3 - x2) * 0.5
        y6 = y2 + (y3 - y2) * 0.5

        # Determine direction of bisection segment s1
        try:
            mr1 = - (x2 - x1) / (y2 - y1)
        except ZeroDivisionError:
            # Horizontal line, ortogonal to segment s1
            mr1 = None

        # Determine direction of bisection segment s2
        try:
            mr2 = - (x3 - x2) / (y3 - y2)
        except ZeroDivisionError:
            # Horizontal line, ortogonal to segment 2
            mr2 = None

        # Determine intersection of bisections
        xo = None
        yo = None
        try:
            yo = (((-y5 * mr2) / mr1) + mr2 * x5 - mr2 * x6 + y6) / (1 - mr2 / mr1)
            xo = (yo - y5 + mr1 * x5) / mr1
        except ZeroDivisionError:
            pass
        except TypeError:
            pass

        if mr1 is None and mr2 != 0:
            # Intersection to horizontal bisection
            xo = x5
            yo = mr2 * (x5 - x6) + y6
        elif mr1 is None and mr2 == 0:
            # Intersection horizontal and vertical bisections
            xo = x5
            yo = y6
        if mr2 is None and mr1 != 0:
            # Intersection to horizontal bisection
            xo = x6
            yo = mr1 * (x6 - x5) + y5
        elif mr2 is None and mr1 == 0:
            # Intersection horizontal and vertical bisections
            xo = x6
            yo = y5
        if mr1 == 0 and mr2 is not None:
            # Intersection to vertical bisection
            yo = y5
            xo = x6 + (y5 - y6) / mr2
        if mr2 == 0 and mr1 is not None:
            # Intersection to vertical bisection
            yo = y6
            xo = x5 + (y6 - y5) / mr1

        geom_center = ogr.Geometry(ogr.wkbPoint)
        geom_center.AddPoint(xo, yo)
        geom_1 = ogr.Geometry(ogr.wkbPoint)
        geom_1.AddPoint(x1, y1)
        radious = geom_center.Distance(geom_1)
        circle_geom = geom_center.Buffer(radious)

        return circle_geom

    def __str__(self):
        text = ("Segment A: %s" % self._segment_a.__str__()
                + "Segment B: %s " % self._segment_b.__str__()
                + "Segment C: %s" % self._segment_c.__str__())
        return text
